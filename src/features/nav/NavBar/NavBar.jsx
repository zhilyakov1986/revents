import React, { Component } from 'react';
import { Menu, Container, Button } from 'semantic-ui-react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import SignedOutMenu from '../Menus/SignedOutMenu';
import SignendInMenu from '../Menus/SignendInMenu';

class NavBar extends Component {
  state = {
    authenticated: false,
  };
  hadleSignedIn = () => this.setState({ authenticated: true });
  hadleSignedOut = () => {
    this.setState({ authenticated: false });
    this.props.history.push('/');
  };
  render() {
    const { authenticated } = this.state;
    return (
      <Menu inverted fixed='top'>
        <Container>
          <Menu.Item as={NavLink} exact to='/' header>
            <img src='assets/logo.png' alt='logo' />
            Re-vents
          </Menu.Item>
          <Menu.Item as={NavLink} to='/events' name='Events' />
          <Menu.Item as={NavLink} to='/people' name='People' />
          <Menu.Item as={NavLink} to='/testing' name='Testing' />
          <Menu.Item>
            <Button
              as={Link}
              to='createEvent'
              floated='right'
              positive
              inverted
              content='Create Event'
            />
          </Menu.Item>
          {authenticated ? (
            <SignendInMenu signOut={this.hadleSignedOut} />
          ) : (
            <SignedOutMenu signIn={this.hadleSignedIn} />
          )}
        </Container>
      </Menu>
    );
  }
}
export default withRouter(NavBar);
